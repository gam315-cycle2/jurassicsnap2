﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Problem : ScriptableObject
{
    [TextArea(3, 10)]
    public string description;
}
