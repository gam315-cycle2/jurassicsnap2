﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MaximizeScript : MonoBehaviour
{
    private GameObject maximizeObj;

    private void Start() 
    {
        maximizeObj = GameObject.FindWithTag("Maximize");
    }

    public void Maximize()
    {
        maximizeObj.GetComponent<RawImage>().enabled = true;
        maximizeObj.GetComponent<RawImage>().texture = this.GetComponent<RawImage>().texture;
    }
}
