﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core;

public class DatabaseManager : MonoBehaviour
{
    public bool databaseEnabled = false;
    private MongoClient client;
    private IMongoDatabase database;
    private IMongoCollection<BsonDocument> pictures;
    private IMongoCollection<BsonDocument> stats;
    private FilterDefinition<BsonDocument> filter;

    [HideInInspector] public List<Stats> myStats = new List<Stats>();
    [HideInInspector] public List<Picture> myPictures = new List<Picture>();
    public class Picture
    {
        public ObjectId _id;

        public string img { get; set; }
        public string tag { get; set; }
    }
    public class Stats
    {
        public ObjectId _id;

        public int total { get; set; }
        public string tag { get; set; }
    }

    private void Start()
    {
        if (!databaseEnabled)
        {
            GenerateStats();
            return;
        }
        client = new MongoClient("mongodb://127.0.0.1/admin");
        database = client.GetDatabase("admin");
        stats = database.GetCollection<BsonDocument>("stats");
        pictures = database.GetCollection<BsonDocument>("pictures");
        filter = Builders<BsonDocument>.Filter.Empty;
        LoadStats();
        LoadPictures();
        //GenerateStats();
    }
    private void GenerateStats()
    {
        bool brokenFence = false;
        bool footPrint = false;
        bool packDino = false;
        bool chaseDino = false;
        foreach (Stats stat in myStats)
        {
            if (stat.tag == "brokenFence")
            {
                brokenFence = true;
            }
            if (stat.tag == "footPrint")
            {
                footPrint = true;
            }
            if (stat.tag == "packDino")
            {
                packDino = true;
            }
            if (stat.tag == "chaseDino")
            {
                chaseDino = true;
            }
        }
        if (!brokenFence)
        {
            Stats temp = new Stats();
            temp.tag = "brokenFence";
            temp.total = 0;
            myStats.Add(temp);
        }
        if (!footPrint)
        {
            Stats temp = new Stats();
            temp.tag = "footPrint";
            temp.total = 0;
            myStats.Add(temp);
        }
        if (!packDino)
        {
            Stats temp = new Stats();
            temp.tag = "packDino";
            temp.total = 0;
            myStats.Add(temp);
        }
        if (!chaseDino)
        {
            Stats temp = new Stats();
            temp.tag = "chaseDino";
            temp.total = 0;
            myStats.Add(temp);
        }
    }
    private void LoadStats()
    {
        List<BsonDocument> result;
        result = stats.Find(filter).ToList();
        foreach (BsonDocument document in result)
        {
            Stats temp = new Stats();
            temp.total = document.GetElement("total").Value.ToInt32();
            temp.tag = document.GetElement("tag").Value.ToString();
            myStats.Add(temp);
        }

    }
    private void LoadPictures()
    {
        List<BsonDocument> result;
        result = pictures.Find(filter).ToList();
        foreach (BsonDocument document in result)
        {
            Picture temp = new Picture();
            temp.img = document.GetElement("img").Value.ToString();
            temp.tag = document.GetElement("tag").Value.ToString();
            myPictures.Add(temp);
        }
    }
    public void SaveToDatabase()
    {
        if (!databaseEnabled)
        {
            return;
        }
        foreach (Stats stat in myStats)
        {
            BsonDocument document = BsonDocument.Create(stat.ToBsonDocument());
            List<BsonDocument> result;
            result = stats.Find(filter).ToList();
            bool pushToDatabase = true;
            foreach (BsonDocument doc in result)
            {
                if (doc.GetElement("tag").Value.ToString() == stat.tag)
                {
                    pushToDatabase = false;
                }
            }
            if (pushToDatabase)
            {
                stats.InsertOne(document.ToBsonDocument());
            }
        }
        foreach (Picture pic in myPictures)
        {
            BsonDocument document = BsonDocument.Create(pic.ToBsonDocument());
            List<BsonDocument> result;
            result = pictures.Find(filter).ToList();
            bool pushToDatabase = true;
            foreach (BsonDocument doc in result)
            {
                if (doc.GetElement("img").Value.ToString() == pic.img)
                {
                    pushToDatabase = false;
                }
            }
            if (pushToDatabase)
            {
                pictures.InsertOne(document.ToBsonDocument());
            }
        }
    }
    public void AddPicture(string aImg, string aTag)
    {
        Picture temp = new Picture();
        temp.img = aImg;
        temp.tag = aTag;
        myPictures.Add(temp);
    }
    public void UpdateDatabaseStats(List<GameObject> aObjects)
    {
        foreach(GameObject obj in aObjects)
        {
            foreach(Stats stat in myStats)
            {
                if(obj.name == stat.tag)
                {
                    ++stat.total;
                }
            }
        }
    }
}
