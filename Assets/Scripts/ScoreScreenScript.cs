﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScreenScript : MonoBehaviour
{
    private GameObject gameManagerObj;
    private DatabaseManager database;
    private LoadPictureScript myLPS;
    [SerializeField] private GameObject contentSizeFitter;
    [SerializeField] private GameObject imagePrefab;
    [SerializeField] private GameObject textPrefab;
    private void Start()
    {
        myLPS = GetComponent<LoadPictureScript>();
        gameManagerObj = GameObject.FindWithTag("GameController");
        database = gameManagerObj.GetComponent<DatabaseManager>();
        database.SaveToDatabase();
        LoadPictures();
        LoadStats();
    }
    private void LoadPictures()
    {
        foreach (DatabaseManager.Picture pic in database.myPictures)
        {
            GameObject temp = Instantiate(imagePrefab);
            temp.transform.parent = contentSizeFitter.transform;
            temp.transform.localPosition = Vector3.zero;
            myLPS.LoadPicture(temp, pic.img);
            Debug.Log(pic.img);
        }
    }
    private void LoadStats()
    {
        for(int i = 0; i < database.myStats.Count; i++)
        {
            GameObject temp = Instantiate(textPrefab);
            temp.transform.position = new Vector3(100,100-(i*30),0);
            temp.GetComponent<Text>().text = database.myStats[i].tag + ": " + database.myStats[i].total;
            temp.transform.parent = GameObject.FindWithTag("Canvas").transform;
        }
    }
    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Prototype");
        }
    }
}
