﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndSceneManager : MonoBehaviour
{
    LoadPictureScript myLPS;
    GameManager instance;
    DatabaseManager database;
    [SerializeField]
    GameObject[] FinalImages;
    private void Start()
    {
        instance = ((GameManager)FindObjectOfType(typeof(GameManager)));
        database = instance.GetComponent<DatabaseManager>();
        myLPS = GetComponent<LoadPictureScript>();
        for (int i = 1; i <= instance.imageCount; i++)
        {
            myLPS.LoadPicture(FinalImages[i - 1], "Image " + i + ".png" , 0);
        }
    }
}
