﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CameraUIScript : MonoBehaviour
{
    [SerializeField] private GameObject recordingDot;
    [SerializeField] private bool recording = true;

    [SerializeField] private TextMeshProUGUI daytimeText;

    [SerializeField] private GameObject batteryCharge;
    [SerializeField] private Transform batteryContent;
    [SerializeField] private int batteryChargeAmount = 4;



    [SerializeField] private TextMeshProUGUI filmText;
    [SerializeField] private int maxFilmAmount = 20;
    private int currentFilmAmount = 0;

    [SerializeField] private GameObject lid;
    [SerializeField] private Transform lidClosedPosition;
    [SerializeField] private Transform lidOpenPosition;

    [SerializeField] private GameObject cameraLight;

    public static CameraUIScript Instance;

    private void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        StartCoroutine(BlinkingDot());
        ChargeBattery();
        currentFilmAmount = maxFilmAmount;
        UpdateFilmText(currentFilmAmount);
    }
    private void Update()
    {
        daytimeText.text = DateTime.Now.ToLongTimeString();

    }


    public void Snap()
    {
        if (currentFilmAmount <= 0)
        {
            //display that you are out of film
            filmText.text = "Return To Outpost!";
            return;
        }
        currentFilmAmount--;
        UpdateFilmText(currentFilmAmount);

        StartCoroutine(Flash());
        Blink();
    }

    private void Blink()
    {
        iTween.MoveTo(lid, iTween.Hash("position", lidClosedPosition.position, "time", 0.1f,
            "oncomplete", "LidHelper",
            "oncompletetarget", gameObject));
    }
    private void LidHelper()
    {
        iTween.MoveTo(lid, iTween.Hash("position", lidOpenPosition.position, "time", 0.1f));
    }



    private IEnumerator Flash()
    {
        cameraLight.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        cameraLight.SetActive(false);
    }
    private void UpdateFilmText(int aAmount)
    {
        filmText.text = string.Format("{0}/{1}", aAmount, maxFilmAmount);
    }

    private void ChargeBattery()
    {
        foreach (Transform t in batteryContent)
        {
            Destroy(t.gameObject);
        }
        for (int i = 0; i < batteryChargeAmount; i++)
        {
            Instantiate(batteryCharge, batteryContent);
        }
        StartCoroutine(DrainBattery());
    }

    private IEnumerator DrainBattery()
    {
        while (true)
        {
            if (batteryContent.childCount == 0) break;


            batteryContent.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.3f);


            if (batteryContent.childCount == 0) break;

            batteryContent.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);



            if (batteryContent.childCount == 0) break;
           
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene("ScoreScreen");
    }

    public void TakeDamage()
    {
        if (batteryContent.childCount == 0) return;
        Destroy(batteryContent.GetChild(0));
    }

    private IEnumerator BlinkingDot()
    {
        while (recording)
        {
            recordingDot.SetActive(true);
            yield return new WaitForSeconds(0.3f);
            recordingDot.SetActive(false);
            yield return new WaitForSeconds(0.3f);

        }

    }


}
