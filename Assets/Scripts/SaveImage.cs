﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SaveImage : MonoBehaviour
{
    [SerializeField]
    private string s;
    ObjectCollectorScript aOCS;
    CameraUIScript aCUIS;
    bool justfired = false;
    //help from https://answers.unity.com/questions/1616300/how-to-correctly-save-a-screenshot-in-a-custom-pat.html
    //keep a count of our screenshots
    GameManager instance;
    int maxFilm = 20;
    [SerializeField]
    [Tooltip("Check this box if you are just going to capture the screen. Otherwise, set to false so that only the attached camera captures an image.")]
    private bool fullScreenShotTurnedOn;
    private Texture2D image;
    //folder to create screen shots and where to store them
    private string screenShotFolder = "JurassicSnap_Data/Resources/Pictures/";
    [SerializeField]
    [Tooltip("This is the resolution width for the camera screen shot.")]
    private int resWidth;
    [SerializeField]
    [Tooltip("This is the resolution height for the camera screen shot.")]

    private int resHeight;
    // Start is called before the first frame update
    void Start()
    {
        instance = (GameManager)FindObjectOfType(typeof(GameManager));
        aOCS = ObjectCollectorScript.Instance;
        aCUIS = CameraUIScript.Instance;

        //if we don't have the directory, make one
        if (!System.IO.Directory.Exists(screenShotFolder))
            System.IO.Directory.CreateDirectory(screenShotFolder);
    }

    // Update is called once per frame
    void Update()
    {
        if(justfired == true)
        {
            if(Input.GetAxis("Fire1") < .3f)
            {
                justfired = false;
            }
        }

        //when you click, save out a screen shot
        if ((Input.GetButtonDown("Fire1") || Input.GetAxis("Fire1") > .3f) && maxFilm > 0 && justfired == false)
        {
            maxFilm--;
            justfired = true;
            if (fullScreenShotTurnedOn)
            {
                FullScreenShot();
            }
            else
            {
                CameraScreenShot();
            }
        }
    }

    void FullScreenShot()
    {
        //add one to the count.
        instance.imageCount++;
        //take a capture of the screen and add it our screen shot directory
        ScreenCapture.CaptureScreenshot((System.IO.Path.Combine(screenShotFolder, "Image " + instance.imageCount + ".png")));
        image = Resources.Load<Texture2D>(screenShotFolder + "Image " + instance.imageCount + ".png");
        byte[] bytes = image.EncodeToPNG();
        s = Convert.ToBase64String(bytes);
        instance.GetComponent<DatabaseManager>().AddPicture(s, "Placeholder");
        Debug.Log("Loaded image!");
        aOCS.Snap();
        aCUIS.Snap();

    }
    //https://answers.unity.com/questions/22954/how-to-save-a-picture-take-screenshot-from-a-camer.html
    void CameraScreenShot()
    {
        instance.imageCount++;
        //make a render texture that has the width, height, and depth that we want
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        //get the camera attached to this game object
        GetComponent<Camera>().targetTexture = rt;
        //set up a screenshot texture that also has the same properties as the rendertexture
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        //render the camera
        GetComponent<Camera>().Render();
        //set the rendertexture to active
        RenderTexture.active = rt;

        
        //read all the pixels in to the screen shot
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        GetComponent<Camera>().targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        //destroy the rendertexture because we no longer need it
        Destroy(rt);
        //write all the bytes out to form a png
        byte[] bytes = screenShot.EncodeToPNG();
        s = Convert.ToBase64String(bytes);
   
        instance.GetComponent<DatabaseManager>().AddPicture(s, "Placeholder");
        //save the name of the screen shot and the location
        string filename = Application.persistentDataPath + "Image " + instance.imageCount + ".png";
        //write the bytes out to our new image file
        System.IO.File.WriteAllBytes(filename, bytes);
        ObjectCollectorScript.Instance.Snap();
        CameraUIScript.Instance.Snap();

    }


}
