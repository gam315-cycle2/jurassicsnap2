﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{

    [SerializeField] private float timeTilStart = 10;

    [SerializeField] private GameObject introText;

    private void Start()
    {
        StartCoroutine(WaitTilStart());
    }

    private void Update()
    {
        introText.transform.position = new Vector3(introText.transform.position.x, introText.transform.position.y + 1, introText.transform.position.z);
        
    }
    private IEnumerator WaitTilStart()
    {
        yield return new WaitForSeconds(25);
        StartGame();
    }


    public void StartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Prototype");
    }

}
