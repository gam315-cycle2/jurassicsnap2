﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class LoadPictureScript : MonoBehaviour
{
    public void LoadPicture(GameObject aRenderer, string aPictureName, int overLoadToDefault)
    {
        Texture2D image = new Texture2D(256, 256);
        byte[] bytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + aPictureName);
        image.LoadImage(bytes);

        //Texture2D image = Resources.Load<Texture2D>("JurassicSnap_Data/Resources/Pictures/" + aPictureName);
        if (image == null)
            Debug.LogError("HECC");
        aRenderer.GetComponent<RawImage>().texture = image;
    }
    public void LoadPicture(GameObject aRenderer, string aPictureString)
    {
        byte[] byteArr = System.Convert.FromBase64String(aPictureString);


        Texture2D image = new Texture2D(256, 256);
        image.LoadImage(byteArr);
        aRenderer.GetComponent<RawImage>().texture = image;

        //Texture2D texture2d = (Texture2D)aRenderer.GetComponent<RawImage>().texture;
        //aRenderer.GetComponent<RawImage>().texture = 
        // var ahrehae = texture2d.LoadImage(byteArr);
        //texture2d.LoadImage(byteArr);

        //aRenderer.GetComponent<RawImage>().texture = texture2d;
    }
}
