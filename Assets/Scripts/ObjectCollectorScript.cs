﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using TMPro;

public class ObjectCollectorScript : MonoBehaviour
{

    public static ObjectCollectorScript Instance;

    public Dictionary<string, int> problemsFound = new Dictionary<string, int>();

    [SerializeField] private string textFileName = "DataToStore.txt";

    [SerializeField] private TextMeshProUGUI snapText;

    [SerializeField] private float captureDistance = 5;


    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void Snap()
    {
        StoreObjectNames(GetObjectsInView());
        //UpdateSnapText();
        GameObject.FindWithTag("GameController").GetComponent<DatabaseManager>().UpdateDatabaseStats(GetObjectsInView());
    }
    //Update our lil UI
    private void UpdateSnapText()
    {
        string text = "";
        foreach (string s in problemsFound.Keys)
        {
            text += string.Format("{0} x{1} \n", s, problemsFound[s]);
        }
        snapText.text = text;
    }

    //Grab all objects in view
    private List<GameObject> GetObjectsInView()
    {
        List<GameObject> objects = new List<GameObject>();

        //Grabs a bunch of renderers and does some frustrum calculations 
        //If its on the screen and has a "Problem" tag, add it to our returned list

        MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer>();
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        for (int i = 0; i < renderers.Length; i++)
        {
            if (GeometryUtility.TestPlanesAABB(planes, renderers[i].bounds))
            {
                if (renderers[i].tag == "Problem" && Vector3.Distance(Camera.main.gameObject.transform.position, renderers[i].transform.position) < captureDistance)
                    objects.Add(renderers[i].gameObject);
                if(renderers[i].tag == "Enemy" && Vector3.Distance(Camera.main.gameObject.transform.position, renderers[i].transform.position) < captureDistance)
                {
                    GetComponent<RaptorMember>().CameraFlash();
                    GetComponent<TRexScript>().CameraFlash();
                }
            }
        }
        return objects;
    }

    //Store the objects in our local data as well as a text doc
    private void StoreObjectNames(List<GameObject> aObjects)
    {
        //Store the object names and iterations in a dictionary
        for (int i = 0; i < aObjects.Count; i++)
        {
            if (problemsFound.ContainsKey(aObjects[i].name))
            {
                problemsFound[aObjects[i].name]++;
            }
            else
            {
                problemsFound.Add(aObjects[i].name, 1);
            }
        }

        //Checks if the file exists
        //if it does, rewrite the text
        //else make a new text file and write in the data

        StreamWriter writer;
        if (File.Exists(textFileName))
        {          
            File.WriteAllText(textFileName, "");
            writer = File.AppendText(textFileName);         
        }
        else
        {
            writer = File.CreateText(textFileName);
        }
        foreach (string s in problemsFound.Keys)
        {
            writer.WriteLine(string.Format("{0} {1} \n", s, problemsFound[s]));
        }
        writer.Close();
    }
}


