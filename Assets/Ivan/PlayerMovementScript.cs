﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.AI;

public class PlayerMovementScript : MonoBehaviour
{
    Rigidbody rb;
    GameManager gm;
    [SerializeField] float walkingForce;
    [SerializeField] float jumpForce;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && Physics.Raycast(transform.position, Vector3.down, 2.0f))
        {
            //Physics.Raycast(transform.position, Vector3.down, 1.0f);
            rb.AddForce(Vector3.up * jumpForce);
        }
    }

    private void FixedUpdate()
    {
        if (gm.joystickControls)
        {
            rb.AddRelativeForce(Vector3.right * Input.GetAxis("L_XAxis_1") * walkingForce);
            rb.AddRelativeForce(Vector3.back * Input.GetAxis("L_YAxis_1") * walkingForce);
        }
        else
        {
            rb.AddRelativeForce(Vector3.right * Input.GetAxis("Horizontal") * walkingForce);
            rb.AddRelativeForce(Vector3.forward * Input.GetAxis("Vertical") * walkingForce);
        }

        rb.velocity = new Vector3(
            rb.velocity.x * 0.9f,
            rb.velocity.y,
            rb.velocity.z * 0.9f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Enemy")
        {
            RaptorMember rm;
            TRexScript tr;
            if(collision.collider.TryGetComponent<RaptorMember>(out rm))
            {
                if(rm.CanDealDamage())
                {
                    CameraUIScript.Instance.TakeDamage();
                }
            }
            else if(collision.collider.TryGetComponent<TRexScript>(out tr))
            {
                if (tr.CanDealDamage())
                {
                    CameraUIScript.Instance.TakeDamage();
                }
            }
        }
    }
}
