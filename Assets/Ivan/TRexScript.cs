﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TRexScript : MonoBehaviour
{
    Animator animus;
    GameObject player;
    bool roaring;
    bool pissy;
    [SerializeField] float roamTimer;
    [SerializeField] float roarTimer;
    [SerializeField] float damageTimer;
    float canDealDamage = 0;
    float rT;
    NavMeshAgent nma;
    Vector3 roamPosition;
    [SerializeField] float distanceToPlayer;
    float distancetoRoamPosition;
    AudioSource roarSound;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        animus = GetComponent<Animator>();
        nma = GetComponent<NavMeshAgent>();
        roarSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        canDealDamage += dt;
        distanceToPlayer = Vector3.Distance(this.transform.position, player.transform.position);        
        if(roaring)
        {
            SetRoarAnimation();
            nma.isStopped = true;
            rT -= dt;
            if(rT < 0)
            {
                roaring = false;
            }
        }
        else if(distanceToPlayer < 5.0f)
        {
            transform.LookAt(player.transform);
            SetAttackAnimation();
            nma.isStopped = true;
        }
        else if (!pissy && !roaring)
        {
            if (rT < 0)
            {
                //roam
                roamPosition = new Vector3(
                    transform.position.x + Random.Range(-50, 50),
                    transform.position.y,
                    transform.position.z + Random.Range(-50, 50));
                Roam(dt);
                rT = roamTimer;
            }
            else
            {
                rT -= dt;
                if(Vector3.Distance(this.transform.position, roamPosition) < 5.0f)
                {
                    SetIdleAnimation();
                    nma.isStopped = true;
                }
            }
        }
        else if(pissy)
        {
            MoveToPlayer(dt);
        }
    }

    private void SetIdleAnimation()
    {
        animus.SetBool("isWalking", false);
        animus.SetBool("isRunning", false);
        animus.SetBool("isEating", false);
        animus.SetBool("isAttacking", false);
        animus.SetBool("isRoaring", false);
        animus.SetBool("isDead", false);
    }

    private void SetMoveAnimation()
    {
        SetIdleAnimation();
        animus.SetBool("isRunning", true);
    }

    private void SetAttackAnimation()
    {
        SetIdleAnimation();
        animus.SetBool("isAttacking", true);
    }

    private void SetRoarAnimation()
    {
        SetIdleAnimation();
        rT = roarTimer;
        animus.SetBool("isRoaring", true);
    }

    private void Roam(float dt)
    {
        SetMoveAnimation();
        nma.SetDestination(roamPosition);
        nma.isStopped = false;
    }

    private void MoveToPlayer(float dt)
    {
        SetMoveAnimation();
        nma.SetDestination(player.transform.position);
        nma.isStopped = false;
    }

    public void CameraFlash()
    {
        if (!roarSound.isPlaying)
        {
            roarSound.Play();
        }
        roaring = true;
        pissy = true;
    }

    public bool CanDealDamage()
    {
        if (canDealDamage > damageTimer)
        {
            canDealDamage = 0;
            return true;
        }
        return false;
    }
}
