﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookScript : MonoBehaviour
{
    GameManager gm;
    Transform parent;
    [SerializeField] float upDownSpeed;
    [SerializeField] float leftRightSpeed;
    [SerializeField] float maxDown;
    [SerializeField] float maxUp;
    [SerializeField] float mouseDamping;
    int invertJoystickLook;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject.pare
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        parent = transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        if(gm.invertJoystickLook)
        {
            invertJoystickLook = -1;
        }
        else
        {
            invertJoystickLook = 1;
        }
    }

    private void FixedUpdate()
    {
        if(gm.joystickControls)
        {
            transform.eulerAngles = new Vector3(
                        xRotationClamp(),
                        transform.eulerAngles.y,
                        transform.eulerAngles.z);

            transform.localEulerAngles = new Vector3(
                transform.localEulerAngles.x,
                0,
                0);

            parent.eulerAngles = new Vector3(
                0,
                parent.eulerAngles.y + (Input.GetAxis("R_XAxis_1") * leftRightSpeed),
                0);
        }
        else
        {
            transform.eulerAngles = new Vector3(
                        xRotationClamp(),
                        transform.eulerAngles.y,
                        transform.eulerAngles.z);

            transform.localEulerAngles = new Vector3(
                transform.localEulerAngles.x,
                0,
                0);

            parent.eulerAngles = new Vector3(
                0,
                parent.eulerAngles.y + ((Input.GetAxis("MouseHorizontal") / mouseDamping) * leftRightSpeed),
                0);
        }
    }

    float xRotationClamp()
    {
        float returnMe;
        if(gm.joystickControls)
        {
            returnMe = transform.eulerAngles.x + ((Input.GetAxis("R_YAxis_1") * upDownSpeed) * invertJoystickLook);
        }
        else
        {
            returnMe = transform.eulerAngles.x + ((-Input.GetAxis("MouseVertical") / mouseDamping) * upDownSpeed);
        }
        //if (returnMe > 85.0f)
        //{
        //    returnMe = 85.0f;
        //}
        //else if (returnMe < -85.0f)
        //{
        //    returnMe = -85.0f;
        //}
        return returnMe;
    }
}
