﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RaptorMember : MonoBehaviour
{
    [SerializeField] bool playerInSight;
    [SerializeField] bool rush;
    [SerializeField] float distanceToPlayer;
    [SerializeField] float detectDistance;
    [SerializeField] float walkingStrength;
    [SerializeField] float pursueDistance;
    [SerializeField] float fleeDistance;
    [SerializeField] float damageTimer;
    float canDealDamage = 0;
    bool scatter = false;
    [SerializeField] float scatterTime;
    float remainingScatterTime;
    Rigidbody rb;
    GameObject player;
    bool receivedTip;
    NavMeshAgent nma;
    Animator animus;

    // Start is called before the first frame update
    void Start()
    {
        playerInSight = false;
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();
        receivedTip = false;
        nma = GetComponent<NavMeshAgent>();
        animus = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        canDealDamage += dt;
        distanceToPlayer = Vector3.Distance(this.transform.position, player.transform.position);
        if (distanceToPlayer < detectDistance && !scatter)
        {
            playerInSight = true;
        }
        if(scatter)
        {
            remainingScatterTime -= dt;
            if(remainingScatterTime < 0)
            {
                scatter = false;
            }
            else
            {
                MoveAwayFromPlayer(dt);
            }
        }
        else if(distanceToPlayer < 5.0f)
        {
            transform.LookAt(player.transform);
            SetAttackAnimation();
            nma.isStopped = true;
        }
        else if (rush)
        {
            MoveToPlayer(dt);
        }
        else if (playerInSight && distanceToPlayer < pursueDistance && distanceToPlayer > fleeDistance)
        {
            SetIdleAnimation();
            nma.isStopped = true;
        }
        else if (playerInSight && distanceToPlayer > pursueDistance)
        {
            MoveToPlayer(dt);
        }
        else if (playerInSight && distanceToPlayer < fleeDistance)
        {
            MoveAwayFromPlayer(dt);
        }
        else if (receivedTip)
        {
            MoveToPlayer(dt);
            receivedTip = false;
        }
    }

    private void SetIdleAnimation()
    {
        animus.SetBool("isWalking", false);
        animus.SetBool("isRunning", false);
        animus.SetBool("isDead", false);
        animus.SetBool("isAttacking", false);
        animus.SetBool("isIdling", false);
    }

    private void SetMoveAnimation()
    {
        SetIdleAnimation();
        animus.SetBool("isRunning", true);
    }

    private void SetAttackAnimation()
    {
        SetIdleAnimation();
        animus.SetBool("isAttacking", true);
    }

    private void MoveAwayFromPlayer(float dt)
    {
        SetMoveAnimation();
        //nma.isStopped = true;
        Vector3 myPosition = transform.position;
        Vector3 playerPosition = player.transform.position;
        Vector3 newHeading = playerPosition - myPosition;
        newHeading = new Vector3(
            -newHeading.x,
            newHeading.y,
            -newHeading.z);
        newHeading = newHeading.normalized * 100.0f;
        nma.SetDestination(myPosition + newHeading);
        nma.isStopped = false;
        //rb.AddForce(newHeading.normalized * walkingStrength * dt);
    }

    private void MoveToPlayer(float dt)
    {
        SetMoveAnimation();
        nma.SetDestination(player.transform.position);
        nma.isStopped = false;
    }

    public void PlayerLocationTip()
    {
        receivedTip = true;
    }

    public void Rush()
    {
        rush = true;
    }

    public bool IsPlayerInSight()
    {
        return playerInSight;
    }

    public void CameraFlash()
    {
        RaptorManager.instance.CameraFlash();
    }

    public void FlashResponse()
    {
        playerInSight = false;
        rush = false;
        receivedTip = false;
        scatter = true;
        remainingScatterTime = scatterTime;
    }

    public bool CanDealDamage()
    {
        if(canDealDamage > damageTimer)
        {
            canDealDamage = 0;
            return true;
        }
        return false;
    }
}
