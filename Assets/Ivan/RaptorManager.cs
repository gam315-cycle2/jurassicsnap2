﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaptorManager : MonoBehaviour
{
    public static RaptorManager instance;
    //[SerializeField] List<GameObject> packObjects;
    [SerializeField] List<RaptorMember> packMembers;
    [SerializeField] int necessaryForces;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        packMembers = new List<RaptorMember>(GetComponentsInChildren<RaptorMember>());
    }    

    // Update is called once per frame
    void Update()
    {
        int musteredForces = MusteredForces();
        if(musteredForces >= necessaryForces)
        {
            foreach (var packMember in packMembers)
            {
                packMember.Rush();
            }
        }
        else if(musteredForces >= 1)
        {
            foreach (var packMember in packMembers)
            {
                if (!packMember.IsPlayerInSight())
                {
                    packMember.PlayerLocationTip();
                }
            }
        }
    }

    private int MusteredForces()
    {
        int returnMe = 0;
        foreach (var packMember in packMembers)
        {
            if (packMember.IsPlayerInSight())
            {
                returnMe++;
            }
        }
        return returnMe;
    }

    public void CameraFlash()
    {
        foreach (var packMember in packMembers)
        {
            packMember.FlashResponse();
        }
    }
}
