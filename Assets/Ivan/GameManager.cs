﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor.SceneManagement;
//using UnityEditor;

public class GameManager : MonoBehaviour
{
    public int imageCount = 0;
    public static GameManager instance;
    public bool joystickControls;
    public bool invertJoystickLook;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
    void Update()
    {
        if (Input.GetButton("Escape"))
        {
            Application.Quit();
            //EditorApplication.qui();
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < imageCount; i++)
        {
            System.IO.File.Delete("Assets/Resources/Pictures/Image " + i + 1);

        }
    }
}
